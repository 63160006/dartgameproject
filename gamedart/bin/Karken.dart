import 'dart:io';
import 'dart:math';
import 'Objects.dart';
import 'Obstruct.dart';
import 'randomAble.dart';

class Karken extends Obstruct implements RandomAble {
  late int x = 0;
  late int y = 0;
  late int life = 100;
 late String symbol="";
  Karken() : super("🐙",0,0);

 
  @override
  int random(int max , int min) {
   Random random = Random();
  return random.nextInt(max)+min;
  }
}