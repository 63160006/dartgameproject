import 'dart:io';
import 'Game.dart';
import 'Goal.dart';
import 'Life.dart';
import 'Objects.dart';
import 'Table.dart';

class Boat extends Objects {
  Table? table;
  Goal? goal;
  var game = Game();
  int win = 0;
  int lose = 0;
  bool winner = true;
  int life = 100;
  int x = 0;
  int y = 0;
  String symbol = "";
  bool finish = false;
  Boat(int x, int y) : super("B", 0, 0) {
    this.symbol = "🚤";
    this.x = x;
    this.y = y;
  }

  String getSymbol() {
    return symbol;
  }

  bool walk(String direction) {
    switch (direction) {
      case 'N':
      case 'w':
        if (canWalk(x, y - 1)) {
          y = y - 1;
          break;
        } else {
          break;
        }
      case 'S':
      case 's':
        if (canWalk(x, y + 1)) {
          y = y + 1;
          break;
        } else {
          break;
        }
      case 'E':
      case 'd':
        if (canWalk(x + 1, y)) {
          x = x + 1;
          break;
        } else {
          break;
        }
      case 'W':
      case 'a':
        if (canWalk(x - 1, y)) {
          x = x - 1;
          break;
        } else {
          break;
        }
    }
    if (isFinish()==true) {
      return true;
    }
    if (isFinish() == true || table!.randomObject() == false) {
      if (table!.randomObject() == true) {
        setWinner(true);
      } else {
        setWinner(false);
        setSymbol("-");
        showLost();
      }
      setFinish(true);
      return false;
    }
    else if (finish == true) {
      return false;
    }
    // table!.randomObject();
     return true;
  }

  bool isFinish() {
    return finish;
  }

  void setFinish(bool status) {
    finish = status;
  }

  bool isWinner() {
    return winner;
  }

  void setWinner(bool status) {
    winner = status;
  }

  bool canWalk(int x, int y) {
    for (Objects f in table!.getObstructList()) {
      if (f.isOn(x, y) && f.runtimeType!=Goal) {
        setWinner(false);////////
        print("rumtimeType: ${f.runtimeType}");
        // setSymbol("💀");
        setSymbol("-");
        showLost();
        f.setSymbol("💀");
        game.showLose();
        setFinish(true);
        return true;
      }
    }
    if (table!.isGolds(x, y) == true && table!.inMap(x, y)) {
      setFinish(true);
      setWinner(true);
      return true;
    }
    return table!.inMap(x, y) && table!.isObstruct(x, y) == false;
  }

  void showLost() {
    print("lose!!");
  }

  bool walkW() {
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }

  void setTable(Table table) {
    this.table = table;
  }

  void checkGoal() {
    if (table!.isGoal(x, y)) {
      game.showWin();
      game.showThankyou();
    }
  }

  void checkLife(int life) {
    if (life < 0) {
      game.showLose();
    }
  }
}
