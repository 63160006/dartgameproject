import 'dart:io';

import 'Objects.dart';
class Obstruct extends Objects {
  int x = 0;
  int y = 0;
  String symbol = "";

  Obstruct(String symbol, int x, int y) : super('', 0, 0) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
  }

  int getX() {
    return x;
  }
  int getY() {
    return y;
  }
  void setX(int x) {
    this.x = x;
  }

  void setY(int y) {
    this.y = y;
  }

  String getSymbol() {
    return symbol;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}