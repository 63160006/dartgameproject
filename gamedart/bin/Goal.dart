import 'dart:io';
import 'dart:math';
import 'Objects.dart';
import 'Table.dart';
import 'randomAble.dart';
class Goal extends Objects implements RandomAble{
  int x = 0;
  int y = 0;
  String symbol="";
  Table? table;
  Goal() :super("🚩", 9, 4);

  String getSymbol() {
    return symbol;
  }

  void setTable(Table table) {
    this.table = table;
  }
  
  @override
  int random(int max, int min) {
    Random random = Random();
  return random.nextInt(max)+min;
  }
}