import 'dart:ffi';
import 'dart:io';

class Objects {
  int x = 0;
  int y = 0;
  String symbol = "";

  Objects(String symbol, int x, int y) {
    this.x = x;
    this.y = y;
    this.symbol = symbol;
  }

  int getX() {
    return x;
  }

  int getY() {
    return y;
  }

  String getSymbol() {
    return symbol;
  }

  void setSymbol(String symbol) {
    this.symbol = symbol;
  }

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }

  void setX(int x) {
    this.x = x;
  }

  void setY(int y) {
    this.y = y;
  }
   void showPosition(){
  print("$x,$y");
 }
}
