import 'dart:math';
import 'Game.dart';
import 'Goal.dart';
import 'Karken.dart';
import 'Obstruct.dart';
import 'Boat.dart';
import 'Objects.dart';
import 'randomAble.dart';
import 'dart:io';

class Table implements RandomAble {
  var width;
  var height;
  Objects? cash;

  List<Objects> objects = [];
  List<Objects> obstructList = [];
  int objCount = 0;
  Karken? karken;
  Obstruct? obstruct;
  Objects? getObject;
  Boat? boat;
  Karken? karken1;
  Karken? karken2;
  Karken? karken3;
  Goal? goal;

  Table() {
    this.width = 10;
    this.height = 5;
  }
  void prepareObject() {
    karken1 = Karken();
    (karken1!).setX(karken1!.random(7, 1));
    (karken1!).setY(karken1!.random(5, 0));

    karken2 = Karken();
    (karken2!).setX(karken2!.random(7, 1));
    (karken2!).setY(karken2!.random(5, 0));

    karken3 = Karken();
    (karken3!).setX(karken3!.random(7, 1));
    (karken3!).setY(karken3!.random(5, 0));
    karken1!.showPosition();
     karken2!.showPosition();
      karken3!.showPosition();

    goal = Goal();
    setKarken(karken1!);
    setKarken(karken2!);
    setKarken(karken3!);
    setGoal(goal!);
    obstructList.add(karken1!);
    obstructList.add(karken2!);
    obstructList.add(karken3!);
  }

  void addObj(Objects obj) {
    objects.add(obj);
    objCount++;
  }

  List getObstructList() {
    return obstructList;
  }

  bool randomObject() {
    for (var element in obstructList) {
      if (boat!.isFinish()) {
        return false;
      }
      int x = random(8, 1);
      int y = random(3, 1);
      element.setX(x);
      element.setY(y);
      goal!.setX(9);
      goal!.setY(goal!.random(5, 0));
      if (boat!.isOn(x, y) && element.runtimeType != Goal) {
        boat?.setWinner(false);
        boat?.setFinish(true);
        element.setSymbol("💀");
        
        cash = element;
        return false;
      }
    }
    return true;
  }

  Objects getCash() {
    return cash!;
  }

  void showCountObj() {
    print(objCount);
  }

  void setBoat(Boat boat) {
    this.boat = boat;
    addObj(boat);
  }

  void setKarken(Objects karken) {
    this.karken = karken as Karken;
    addObj(karken);
  }

  void printSymbol(int x, int y) {
    var symbol = '-';
    for (int i = 0; i < objCount; i++) {
      if (objects[i].isOn(x, y)) {
        symbol = objects[i].getSymbol();
      }
    }
    stdout.write("   $symbol   ");
  }

  void clearScreen() {
    print("\x1B[2J\x1B[0;0H");
  }

  void showMap() {
    // clearScreen();
    // if (boat!.isFinish() == true) {
    //   boat!.setSymbol("💀");
    //   boat?.setFinish(true);
    // }
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        printSymbol(x, y);
      }
      print("\n");
    }
  }

  void showTable() {
    showMap();
  }

  void setTable(Table table) {
    table = table;
  }

  void showBoat() {
    print(boat!.getSymbol());
  }

  bool isGoal(int x, int y) {
    return goal!.isOn(x, y);
  }

  void setGoal(Goal goal) {
    this.goal = goal;
    addObj(goal);
  }

  void showGoal() {
    print(goal!.getSymbol());
  }

  void showKarken() {
    print(karken!.getSymbol());
  }

  bool inMap(int x, int y) {
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isObstruct(int x, int y) {
    for (int i = 0; i < objects.length; i++) {
      if (objects[i].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  Object isGolds(int x, int y) {
    return goal!.isOn(x, y);
  }

  @override
  int random(int max, int min) {
    Random random = Random();
    return random.nextInt(max) +min;
  }
}
