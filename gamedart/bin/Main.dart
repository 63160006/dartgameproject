import 'dart:ffi';
import 'dart:io';
import 'Boat.dart';
import 'Game.dart';
import 'Karken.dart';
import 'Table.dart';
import 'Goal.dart';

void main(List<String> args) {
  var game = Game();
  var table = Table();
  var boat = Boat(0, 0);
  table.setBoat(boat);
  table.prepareObject();
  boat.setTable(table);
  game.showWelcome();
  game.showDescription();

  while (boat.isFinish() != true) {
    table.showMap();
    // W,a| N,w| E,d| S, s| q: quit
    String direction = stdin.readLineSync()!;
    if (direction == 'q' || direction == 'quit') {
      return;
    }
    boat.walk(direction);
    //print("\x1B[2J\x1B[0;0H");
  }
  table.showMap();
  if (boat.isWinner() == true) {
    game.showWin();
  } else {
    game.showLose();
  }
  game.showThankyou();
}
